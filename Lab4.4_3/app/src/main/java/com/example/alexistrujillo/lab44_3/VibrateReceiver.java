package com.example.alexistrujillo.lab44_3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.util.Log;

public class VibrateReceiver extends BroadcastReceiver {
    public void onReceive(Context ctx, Intent i) {
        Vibrator mVibrator = (Vibrator) ctx.getSystemService(Context.VIBRATOR_SERVICE);
        mVibrator.vibrate(500);
        Log.d("MENSAJE","Se llamo a Vibrator");
    }
}
